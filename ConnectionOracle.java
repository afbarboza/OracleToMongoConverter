package connectionoraclemongo;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alex Frederico Ramos Barboza
 */
public class ConnectionOracle {
    Connection connection;
 
    
    private static ConnectionOracle instanceOfConnectionOracle;
    
    private ConnectionOracle() {
        
    }
    
    public static synchronized ConnectionOracle getInstance() {
        if (instanceOfConnectionOracle == null) {
            instanceOfConnectionOracle = new ConnectionOracle();
        }
        return instanceOfConnectionOracle;
    }
    
    /**
     * *
     * @return: true, if connected.
     *          false, otherwise.
     */
    public boolean connectOracle()
    {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@grad.icmc.usp.br:15215:orcl",
                    "L7986480",
                    "kernel128");
            
            System.out.println("Bases de Dados conectado com sucesso!");
            return true;
        } catch (ClassNotFoundException ex) {
            System.err.println("Problema: verifique o driver do banco de dados");
            return false;
        } catch(SQLException ex){
            System.err.println("Problema: verifique seu usuário e senha");
            return false;
        }
        
    }
    
    /**
     * 
     * @return: true, if disconnected.
     *          false, otherwise.
     */
    public boolean disconnectOracle()
    {
        try {
            connection.close();
            System.out.println("Desconectando da base de dados\n");
            return true;
        } catch(SQLException ex) {
            System.err.println("Problema ao desconectar a base de dados\n");
            return false;
        }
    }
    
    public String getDatabaseName()
    {
        
        return "futebol";
    }
    
    /**
     * 
     * @return an array list storing the name of each table in out database
     */
    public ArrayList getTableNames()
    {
        ResultSet rs;
        String s = null;
        Statement stmt;
        ArrayList tableNames = new ArrayList();

         try {
            s = "SELECT table_name FROM user_tables ORDER BY TABLE_NAME";
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            while (rs.next()) {
                tableNames.add(rs.getString("table_name"));                
            }
            
            stmt.close();
            return tableNames;
        } catch (SQLException ex) {
            System.err.println("Problema ao consultar a base de dados");
            return null;
        }   
    }
    
    /**
     * getColumnNames: given an @tableName, get the names of each column
     * @param tableName: the name of our table
     * @return : an array list storing the _name_ (but not the values) of each column
     */
    public ArrayList getColumnNames(String tableName)
    {
        ResultSet rs;
        String s = null;
        Statement stmt;
        ArrayList columnNames = new ArrayList();
        
        /* query for get the column name given an table */
        s = "SELECT CNAME FROM COL WHERE TNAME LIKE '" +tableName + "'";
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            /* walk our set, getting the name of each column and adding in our list */
            while (rs.next()) {
                columnNames.add(rs.getString("cname"));
            }
            stmt.close();
            return columnNames;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * getSetValues:    given a table name @tableName and a column name @columnName,
     *                  returns all values stored in the given column of the given table.
     * @param tableName
     * @param columnName
     * 
     * @return an array list, which must store all the values stored in the column
     * @todo TEST THIS CODE
     */
    public ArrayList getSetValues(String tableName, String columnName)
    {
        ResultSet rs;
        String s = null;
        Statement stmt;
        ArrayList valuesOfColumn = new ArrayList();
        
        s = "SELECT " + columnName + " FROM " + tableName + " ORDER BY ROWID";
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            while (rs.next()) {
                valuesOfColumn.add(rs.getString(columnName));
            }
            stmt.close();
            return valuesOfColumn;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    /**
     * isPrimaryKey: checks whether a given @columnName is primary key or compose a primary key
     * @param tableName
     * @param columnName
     * @return true, if is primary key.
     */
    public boolean isPrimaryKey(String tableName, String columnName)
    {
        ResultSet rs;
        String s;
        Statement stmt;
        int numberOfResults = 0;
        
        s = "SELECT TABLE_NAME, COLUMN_NAME FROM USER_CONS_COLUMNS  "
                + "WHERE TABLE_NAME LIKE '" + tableName + "' AND  CONSTRAINT_NAME LIKE 'PK%' "
                + " AND COLUMN_NAME = '" + columnName + "'"
                + " ORDER BY TABLE_NAME, POSITION";
        
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            
            while (rs.next()) {
                numberOfResults++;
            }
            stmt.close();
            if (numberOfResults == 1) {
                return true;
            } else {
                System.err.println("error: ResultSet violates integrity constraint");
                System.exit(numberOfResults);
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * countPrimaryKeys: count the number of primary keys in a table 
     * @param tableName
     * @return the number of primary keys in a table ( > 1 if composite).
     */
    public int countPrimaryKeys(String tableName)
    {
        ResultSet rs;
        String s;
        Statement stmt;
        int numberOfResults = 0;
        
        s = "SELECT TABLE_NAME, COLUMN_NAME FROM USER_CONS_COLUMNS  "
                + "WHERE TABLE_NAME LIKE '" + tableName + "' AND  CONSTRAINT_NAME LIKE 'PK%' "
                + " ORDER BY TABLE_NAME, POSITION";
        
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            
            while (rs.next()) {
                numberOfResults++;
            }
            stmt.close();
            return numberOfResults;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    /**
     * isForeignKey: checks whether a given @columnName is foreign key or compose a foreign key.
     * @param tableName
     * @param columnName
     * @return true, if it is.
     *          false, otherwise
     */
    public boolean isForeignKey(String tableName, String columnName)
    {
        String s;
        ResultSet rs;        
        Statement stmt;
        int numberOfResults = 0;
        
        s = "SELECT TABLE_NAME, COLUMN_NAME FROM USER_CONS_COLUMNS  "
                + "WHERE TABLE_NAME LIKE '" + tableName + "' AND  CONSTRAINT_NAME LIKE 'FK%' "
                + " AND COLUMN_NAME = '" + columnName + "'"
                + " ORDER BY TABLE_NAME, POSITION";
        
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            
            while (rs.next()) {
                numberOfResults++;
            }
            stmt.close();
            if (numberOfResults == 1) {
                return true;
            } else {
                System.err.println("error: ResultSet violates integrity constraint");
                System.exit(numberOfResults);
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * countForeignKeys: count the number of foreign keys in a table
     * @param tableName
     * @return the number of foreign keys in a table
     */
    public int countForeignKeys(String tableName)
    {
        ResultSet rs;
        String s;
        Statement stmt;
        int numberOfResults = 0;
        
        s = "SELECT TABLE_NAME, COLUMN_NAME FROM USER_CONS_COLUMNS  "
                + "WHERE TABLE_NAME LIKE '" + tableName + "' AND  CONSTRAINT_NAME LIKE 'FK%' "
                + " ORDER BY TABLE_NAME, POSITION";
        
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            
            while (rs.next()) {
                numberOfResults++;
            }
            stmt.close();
            return numberOfResults;
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    /**
     * mapToOracleDatatypes: given a string representation of a Oracle datatype, return its 'Java ' representation
     * @param strDatatype: the java-string storing the column type
     * @return : the OracleDatatype equivalent
     */
    private OracleDatatypes mapToOracleDatatypes(String strDatatype) 
    {
        switch (strDatatype) {
            case "CHAR":
                return OracleDatatypes.ORACLE_CHAR;
            case "VARCHAR":
                return OracleDatatypes.ORACLE_VARCHAR;
            case "VARCHAR2":
                return OracleDatatypes.ORACLE_VARCHAR2;
            case "NUMBER":
                return OracleDatatypes.ORACLE_NUMBER;
            case "DATE":
                return OracleDatatypes.ORACLE_DATE;
            case "BLOB":
                return OracleDatatypes.ORACLE_BLOB;
            default:
                return OracleDatatypes.ORACLE_INVALID;
        }
    }
    
    /**
     * getColumnType: finds the Oracle-type of a given column
     * @param tableName: the name of the table storing the column
     * @param columnName: the column name
     * @return the OracleDatatype equivalent
     */
    public OracleDatatypes getColumnType(String tableName, String columnName)
    {
        String s;
        ResultSet rs;
        Statement stmt;
        int numberOfResults = 0;
        String strDatatype = null;
        
        s = "SELECT * FROM COL WHERE TNAME = '" + tableName + "' AND CNAME = '" + columnName + "'";
        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(s);
            
            while (rs.next()) {
                strDatatype = rs.getString("COLTYPE");
            }
            
            return mapToOracleDatatypes(strDatatype);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
            return OracleDatatypes.ORACLE_INVALID;
        }
    }
    
    /**
     * getConstraints - gets the set of constraints of a given field
     * @param tableName: the table where the field belongs to.
     * @param columnName: the column where the field belongs to.
     * @return : an array list storing the java string representation of constraints,
     */
    public ArrayList<String> getConstraints(String tableName, String columnName)
    {
        String s;
        ResultSet rs;
        Statement stmt;
        ArrayList<String> columnConstraints = new ArrayList();
        
        s = "SELECT * FROM USER_CONS_COLUMNS \n" +
            "WHERE TABLE_NAME LIKE '" + tableName+ "' AND COLUMN_NAME LIKE '"+ columnName + "'" +
            "AND (CONSTRAINT_NAME LIKE 'FK%' OR CONSTRAINT_NAME LIKE 'PK%')\n" +
            "ORDER BY TABLE_NAME, COLUMN_NAME, POSITION";

        try {
           stmt = connection.createStatement();
           rs = stmt.executeQuery(s);
           
           while (rs.next()) {
                columnConstraints.add(rs.getString("CONSTRAINT_NAME"));
           }
           return columnConstraints;
        } catch (SQLException ex) {
                Logger.getLogger(ConnectionOracle.class.getName()).log(Level.SEVERE, null, ex);
                return null;
        }
    }
}
