package connectionoraclemongo;

import com.mongodb.*;
import java.util.ArrayList;

/**
 * @author alex
 */
public class ConnectionMongo {
    private static ConnectionMongo instanceOfConnectionMongo;
    private MongoClient mongoClient;
    private DB mongoDB;
    private ArrayList<DBCollection> tables;
    
    private ConnectionMongo(){
        
    }
    
    public static synchronized ConnectionMongo getInstance()
    {
        if (instanceOfConnectionMongo == null) {
            instanceOfConnectionMongo = new ConnectionMongo();
        }
        return instanceOfConnectionMongo;
    }
    
    /**
     * connects the client application with server, and creates a new db instance
     * 
     * @param databaseName: the name of the new db
     */
    public void connectMongo(String databaseName) throws MongoException
    {
        this.mongoClient = new MongoClient("localhost", 65535);
        this.mongoDB = mongoClient.getDB(databaseName);
        if (this.mongoDB.getMongo() == null) {
            throw new MongoException("Connection to the server failed.\n");
        }
    }
    
    /**
     * createCollections: create collections in our mongoDB
     * 
     * @param collectionNames: tha names of the collections to be created
     */
    public void createCollections(ArrayList collectionNames)
    {
        if (collectionNames == null) {
            System.err.println("nome de tabelas inválido");
        }
        
        tables = new ArrayList();
        for (Object o : collectionNames) {
            String tmpTableName = (String) o;
            tables.add(mongoDB.getCollection(tmpTableName));
        }
    }
    
    public DBCollection getCollection(String tableName)
    {
        return this.mongoDB.getCollection(tableName);
    }
       
    public void disconnectMongo()
    {
        this.mongoClient.close();
    }
}
