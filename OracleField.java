package connectionoraclemongo;

import java.util.ArrayList;

/**
 *
 * @author alex
 */
public class OracleField {
    /* the column name where the field belongs */
    private final String fieldLabel;
    
    /* the Oracle Datatype where the field belongs */
    private final OracleDatatypes fieldType;
    
    /* the value stored in the field */
    private final String fieldValue;
    
    /* the set of constraints above this field */
    private final ArrayList<String> constraintName;
    
    /* flag indicating whether the field compose a primary key*/
    private final boolean primaryKey;
    
    /*flag indicating whether the field compose a foreign key */
    private final boolean foreignkey;

    public OracleField(String fieldLabel, OracleDatatypes fieldType, String fieldValue, ArrayList<String> constraintName, boolean primaryKey, boolean foreignkey) {
        this.fieldLabel = fieldLabel;
        this.fieldType = fieldType;
        this.fieldValue = fieldValue;
        this.constraintName = constraintName;
        this.primaryKey = primaryKey;
        this.foreignkey = foreignkey;
    }

    public String getFieldValue() {
        return fieldValue;
    }
    
    public String getFieldLabel() {
        return fieldLabel;
    }

    public OracleDatatypes getFieldType() {
        return fieldType;
    }

    public ArrayList<String> getConstraintName() {
        return constraintName;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public boolean isForeignkey() {
        return foreignkey;
    }

    /**
     * checks whether the field gives has one or more constraints
     * @return true, if it has.
     *          false, otherwise.
     */
    public boolean hasConstraints() {
        return (this.constraintName.isEmpty());
    }
}
