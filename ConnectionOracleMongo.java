package connectionoraclemongo;

import com.mongodb.MongoException;
import com.mongodb.MongoSocketOpenException;
import java.util.ArrayList;

/**
 *
 * @author alex
 */
public class ConnectionOracleMongo {

    public void fillOracleFields(String tableName, String columnName)
    {
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConnectionOracle cn = ConnectionOracle.getInstance();
        cn.connectOracle();
        ArrayList tables = cn.getTableNames();
        ArrayList columnNames = null;
        
        for (int i = 0; i < tables.size(); i++) {
            String tmpTableName = (String) tables.get(i);
            columnNames = cn.getColumnNames(tmpTableName);
            
            System.out.println(tmpTableName + ":\t");
            for (int j = 0; j < columnNames.size(); j++) {
                String tmpColumnName = (String) columnNames.get(j);
                System.out.println("\t\t-> " + tmpColumnName);
            }
        }
               
        System.out.println(cn.getColumnType("F15_GOLS_CARTOES", "IDPARTIDA"));
        System.out.println(cn.getColumnType("F12_PATROCINA", "CONTRATO"));
        System.out.println(cn.getColumnType("F11_PARTIDA", "DATAHORA"));
        System.out.println(cn.getColumnType("F13_APITA", "FUNCAO"));
        
        /*ConnectionMongo mongo = ConnectionMongo.getInstance();
        mongo.connectMongo(cn.getDatabaseName());
        mongo.createCollections(columnNames); */       
        ArrayList<String> cons = cn.getConstraints("F02_CIDADE", "SIGLAC");
        for (String s : cons) {
            System.out.println(s);
        }
        
        cn.disconnectOracle();
    }
}
