/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectionoraclemongo;

/**
 *
 * @author alex
 */
public enum OracleDatatypes {
   ORACLE_CHAR(1), ORACLE_VARCHAR(2), ORACLE_VARCHAR2(3), ORACLE_NUMBER(4), ORACLE_DATE(5), ORACLE_BLOB(6), ORACLE_INVALID(-1);
   
   private final int value;
   private OracleDatatypes(int value) {
        this.value = value;
    }
}
